# USB UII LOG




![USB_UII_LOG](usb_uii_log.jpg)



Program pre čítanie dát z modulu USB_UII_LOG a ukladanie dát do *.csv súboru v node-red.



USB loger na meranie napätia a prúdu. Hardware pozostáva z dosky Arduino Nano, modul 16-bit ADC prevodníka ADS1115, a dva izolované prúdové snímače ACS712-20. 



**Špecifikácia:**

- Napäťový rozsah: 0-20V

- prúdový rozsah: -20 - +20 A